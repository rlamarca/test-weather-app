
# Hearst Weather Sample App; 

## General Notes: 
Based on our discussions last week, I decided to make this a basic app, but also one that would 
use some of the technologies we discussed, especially, Webpack and ES6 javascript.

While it is impossible without seeing the Hearst code bases directly, I think this project makes 
some reasonable assumptions in terms of code strategies at Hearst.

**To this end, I chose a Webpack starter kit for a no-framework application:**

**I chose to avoid ANY external dom libraries, including jquery.** 
I did this partly because most jquery work i would use has native commands, except the jsonp calls.

**I wrote two main pieces of library code, Ajax and Store:**
While these libraries were written with the deadline first in mind, they also have 
some convenience features and a semantic style that _could_ be used for a larger project. 

### Class Accessor functions: 
Ajax and Store classes ( and WeatherStore substore ) both use getter and getter/setter functions,
*but not the native getter and setter functions*.

In other projects I have used similar functions in a way that incorporates a factory method 
to generate accessors from options objects, which aids in brevity and speed of coding. 
For this piece, I just wrote in the actual functions manually.    

examples:

**getter example ( WeatherStore )**
```
    weatherStore.city(); // returns the City name from the weather store current city.
```

**setter preset value example**
```
    ajax.http(); // sets the protocol to http and returns the ajax instance for continuation
```

**setter arbitrary value example**
```
    ajax.port( 8000 ); // sets the port to 8000 and returns the ajax instance for continuation.
    
    // more: 
    ajax.jsonp().host('google.com').port(3000).... 
```

**getter / setter example**
```
    ajax.url( 'persons' ); // sets the url to 'persons' and returns the ajax instance for continuation.
    ajax.url() // returns an assebled full url ( protocol://host:port/url 

```
  
## Part 1

**html templates were used to construct components**.

