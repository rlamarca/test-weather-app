
export const createWeatherComponent = ( weatherStore ) => {
  const cityWeatherContainerComponent =
    document.querySelector( '#city-weather-container');

  const cityWeatherTemplate = document.querySelector('#city-weather');
  const cityWeatherComponent = cityWeatherTemplate.content.cloneNode( true );
  const cityWeatherUlElement = cityWeatherComponent.querySelector( 'ul');
  const weatherMomentComponent = createWeatherMomentComponent( weatherStore );
  cityWeatherUlElement.appendChild( weatherMomentComponent );
  cityWeatherContainerComponent.appendChild( cityWeatherComponent );


  const cityForecastDaysAvailable = weatherStore.cityForecastDaysAvailable();

  let i = 0;
  for ( ; i !== cityForecastDaysAvailable; i++ ) {
    const weatherDayComponent = createWeatherDayComponent( weatherStore, i );
    cityWeatherUlElement.appendChild( weatherDayComponent );
  }
};

const createWeatherMomentComponent = ( weatherStore ) => {

  const cityWeather = weatherStore.cityWeather();
  const cityMomentWeather = weatherStore.cityCurrentWeather();
  const city = weatherStore.city();

  const template = document.querySelector( '#weather-moment-template' );
  const component = template.content.cloneNode( true );
  const titleElement = component.querySelector('.city-weather-header');
  const conditionElement = component.querySelector('.condition');
  const tempElement = component.querySelector('.temp');

  titleElement.textContent = `Current Weather in ${city}`;
  conditionElement.textContent = `Conditions: ${ cityMomentWeather.condition }`;
  tempElement.textContent = `Temperature: ${ cityMomentWeather.temp} \u00B0`;

  return component;
};

const createWeatherDayComponent = ( weatherStore, daysAheadIndex ) => {
  const cityDayWeather = weatherStore.cityDayWeather( daysAheadIndex );
  const city = weatherStore.city();

  console.log( cityDayWeather );
  const template = document.querySelector( '#weather-day-template');
  const component = template.content.cloneNode( true );
  const titleElement = component.querySelector( '.city-weather-header' );
  const conditionElement = component.querySelector( '.condition' );

  titleElement.textContent = `Weather for ${cityDayWeather.day}, ${cityDayWeather.date}`;
  conditionElement.textContent = `Conditions: ${ cityDayWeather.daycondition }`;

  return component;
};
