
require('normalize.css/normalize.css');
require('./styles/index.scss');

import { main } from './app';

document.addEventListener("DOMContentLoaded", () => {
    main();
});
