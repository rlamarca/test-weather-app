/**
 * gets a property nested in an object by an array of keys.
 * @param object
 * @param keys
 * @returns {*}
 */
import {isEqual} from './comparator.methods';

export const getPropertyAt = ( object, ...keys ) => {
  if ( typeof object !== 'object' ) {
    throw new Error( `cannot get property ${keys[ 0 ] }. object undefined`)
  }

  if ( !keys ) return object;

  const result = object[ keys[ 0 ] ];
  if ( keys.length === 1 ) return result;

  return getPropertyAt( result, ...keys.slice( 1 ) );
};


/**
 * gets a list  of properties an ...keys within a list of objects.
 * ( make this a nesting operation later on )
 * ( also maybe include sets and objects )
 * ( AND probably should set up proper for loop for better perf )
 * @param list { array } the origin list.
 * @param keys : the key route within the origin list
 */
export const getPropertyList = ( list, ...keys ) => {
  if ( Array.isArray( list ) === false ) {
    throw new Error( 'cannot get property list from non-array ' );
  }

  return list.map(
    item => getPropertyAt( item, ...keys )
  );
};

/**
 * filters a list of properties
 * ( make this a nesting operation later on )
 * ( also maybe include sets and objects )
 * ( AND probably should set up proper for loop for better perf )
 * @param list
 * @param keys
 * @param comparator
 * @param comparatorMethod
 * @returns {*}
 */
export const filterListByProperty =
  ( list, keys, comparator, comparatorMethod = isEqual ) => {

  if ( Array.isArray( list ) === false ) {
    throw new Error( 'cannot get property list from non-array' );
  }

  const result = list.filter( item => {
    return comparatorMethod( comparator, getPropertyAt( item, ...keys ) )
  });
  return result;

};
