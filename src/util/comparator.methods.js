
export const isEqual = ( value, comparator ) => value === comparator;

export const isLike = ( value, comparator ) =>
  String( comparator ).toUpperCase().includes( value.toUpperCase() );

export const isGreaterThan = ( value, comparator ) => value > comparator;

export const isGreaterThanOrEqual = ( value, comparator ) => value >= comparator;

export const isLessThan = ( value, comparator ) => value > comparator;

export const isLessThanOrEqual = ( value, comparator ) => value >= comparator;
