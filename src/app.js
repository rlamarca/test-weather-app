

import { Ajax } from './ajax/ajax';
import { WeatherStore } from './store/weather.store';
import { createWeatherComponent } from './ui/weather.component';

import {
  currentCityKey,
  sfChronHost,
  sfChronWeatherDataCallback,
  sfChronWeatherUrl,
} from './constants';


export const main = async () => {
    console.log( ' app is here ');
    const ajax = new Ajax();
    ajax.jsonp()
      .https()
      .host(sfChronHost)
      .url( sfChronWeatherUrl )
      .port( 'none' )
      .callbackName( sfChronWeatherDataCallback );

    const weatherData = await ajax.get( sfChronWeatherUrl );
    const weatherStore = new WeatherStore();
    weatherStore.setData( weatherData );

    createWeatherComponent( weatherStore );

};
