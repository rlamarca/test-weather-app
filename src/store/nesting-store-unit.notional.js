
/** just daydreaming **/
class StoreReducer {

  constructor() {
    this._reducers = {
      onSet: ( data ) => data,
      onGet: ( data ) => data,
    }
  }

  onSet( data ) {
    // return this.
  }

}

export class NestingStoreUnitNotional {

  static TYPE = 'STORE_UNIT';
  TYPE = 'STORE_UNIT';

  constructor() {
    this._data = {};
    this._meta = {
      name: 'blank store unit',
      children: 'ALL',

    }
  }

  data( data ) {
    if ( !data ) return this._data;

    this._data = data;
    return this;
  }


}
