
import {
  filterListByProperty,
  getPropertyAt,
  getPropertyList,
} from '../util/list.methods';

import { isEqual } from '../util/comparator.methods';

const isStore = ( obj ) => obj.BASE_TYPE === 'STORE';


/**
 * This is just a simplified version of what should become a nesting store unit
 * with observers and listeners.
 */
export class Store {

  BASE_TYPE = 'STORE';
  TYPE = 'STORE';

  constructor( data ) {
    this._meta = {
      name: '',
      observers: {
        onGet: ( self, data ) => true,
        onSet: ( self, data ) => true,
      },
      reducers: {
        onGet: ( self, data ) => data,
        onSet: ( self, data ) => data,
      },
    };

    this._data = data;
  }

  /** also should make this
   * object based in the future.
   * @param listKeys
   * @param itemKeys
   * @param comparator
   * @param comparatorMethod
   */
  filterByPropertyListAt( listKeys, itemKeys, comparator, comparatorMethod = isEqual ) {
    return filterListByProperty(
      this.getPropertyAt( listKeys ),
      itemKeys,
      comparator,
      comparatorMethod,
    );
  }

  /**
   * get the data and fire the reducers and listeners
   * @param data
   * @returns {*}
   */
  getData() {
    const { _meta } = this;
    const data = _meta.reducers.onGet( this, this._data );
    _meta.observers.onGet( this, data );
    return data;
  }

  /**
   * set the data and fire the reducers and listeners
   * @param data
   * @returns {*}
   */
  setData( data ) {
    const { _meta } = this;
    this._data = _meta.reducers.onSet( this, data );
    _meta.observers.onSet( this, data );
    return data;
  }

  getPropertyAt( ...keys ) {
    return getPropertyAt( this._data, ...keys );
  }

  getPropertyListAt( listKeys, ...itemKeys ) {
    return getPropertyList(
      this.getPropertyAt( listKeys ),
      ...itemKeys,
    );
  }

  /**
   * shallow merge of data.
   * Will fire reducers and listeners
   * @param data
   * @returns {*}
   */
  mergeData( data ) {
    data = { ...this._data, ...data };
    return this.setData( data );
  }

  /**
   * get or set the getObserver
   * @param callback
   * @private
   */
  __getObserver__( callback ) {
    if ( !callback ) return this._meta.observers.onGet;

    this._meta.observers.onGet = callback;
    return this;
  }

  __setObserver__( callback ) {
    if ( !callback ) return this._meta.observers.onSet;

    this._meta.observers.onSet = callback;
    return this;
  }

  __getReducer__( callback ) {
    if ( !callback ) return this._meta.reducers.onGet;

    this._meta.reducers.onGet = callback;
    return this;
  }


  __setReducer__( callback ) {
    if ( !callback ) return this._meta.observers.onSet;

    this._meta.reducers.onSet = callback;
    return this;
  }

  __makeAccessor__( key ) {
    this[ key ] = function ( value = null ) {
      if ( value === null ) return this._data[ key ];

      this._data[ key ] = value;
      return this;
    }
  }

  __name__( name ) {
    if ( !name ) return this._meta.name;

    this._meta.name = name;
    return this;
  }



}
