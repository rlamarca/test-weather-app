
import { Store } from './store';
import { currentCityKey } from '../constants';

/**
 * Yes, I know I should probably set up
 * some kinda caching for these getter functions
 */
export class WeatherStore extends Store {
  BASE_TYPE = 'STORE';
  TYPE = 'WEATHER_STORE';

  constructor( ...p ) {
    super( ...p );
    this.__name__( 'weather' );
    this._meta = {
      ...this._meta,
      currentCityKey,
    };
  }

  city() {
    const cityData = this.cityWeather();
    if ( !!cityData ) return cityData.geoloc.city;
  }

  cityWeather( citykey = this._meta.currentCityKey ) {
    return this.filterByPropertyListAt(
      ['cities'], ['citykey'], citykey
    )[ 0 ];
  }

  cityCurrentWeather( citykey = this._meta.currentCityKey ) {
    return this.cityWeather( citykey ).current[ 0 ];
  }

  cityDayWeather( daysAheadIndex = 0, citykey = this._meta.currentCityKey ) {
    console.log( citykey );
    // weekly weather seems to start with previous day
    const cityWeather = this.cityWeather( citykey );
    console.log( cityWeather );
    return this.cityWeather( citykey ).weekly[ daysAheadIndex + 2 ];
  }

  cityNameList() {
    return this.getPropertyListAt(
      [ 'cities' ], 'geoloc', 'city'
    );
  }

  cityKeyList() {
    return this.getPropertyListAt(
      [ 'cities' ], 'citykey'
    );
  }

  /**
   *
   * @param citykey
   * @returns {number}
   */
  cityForecastDaysAvailable( citykey = this._meta.currentCityKey ) {
    // weekly weather seems to start with previous day
    return this.cityWeather( citykey ).weekly.length - 2;
  }

  __currentCityKey__( key = null ) {
    if ( key === null ) return this._meta.currentCityKey;

    this._meta.currentCityKey = key;
    return this;
  }

}
