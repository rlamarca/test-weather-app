
import {
  AJAX_OPTIONS,
  DELETE_AJAX_OPTIONS,
  GET_AJAX_OPTIONS,
  POST_AJAX_OPTIONS,
  PUT_AJAX_OPTIONS,
} from './ajax.defaults';
import { encodeGetParams } from './ajax.util';

export class Ajax {

  static TYPE = 'AJAX';
  TYPE = 'AJAX';

  constructor() {
    this._ajaxOptions = {
      AJAX_OPTIONS,
      DELETE_AJAX_OPTIONS,
      GET_AJAX_OPTIONS,
      POST_AJAX_OPTIONS,
      PUT_AJAX_OPTIONS,
    };
    this._currentOptions = GET_AJAX_OPTIONS;
    this._port = 80;
    this._host = 'localhost';
    this._protocol = 'http';
    this._format = 'jsonp';
    this._callbackName = null;
  }

  async get( url = null, params = {}, options = {} ) {
    this._switchOptions( this._ajaxOptions.GET_AJAX_OPTIONS );
    if ( url === null ) return this;

    const paramKeys = Object.keys( params );
    if ( paramKeys < 1 ) {
      return await this.request( url, params, options );
    }

    return await this.request( url, params, options );
  }

  async post( url = null, params = {}, options = {} ) {
    this._switchOptions( this._ajaxOptions.POST_AJAX_OPTIONS );
    if ( url === null ) return this;

    return await this.request( url, params, options );
  }

  async put( url = null, params = {}, options = {} ) {
    this._switchOptions( this._ajaxOptions.PUT_AJAX_OPTIONS );
    if ( url === null ) return this;

    return await this.request( url, params, options );
  }

  async remove( url = null, params = {}, options = {} ) {
    this._switchOptions( this._ajaxOptions.DELETE_AJAX_OPTIONS );
    if ( url === null ) return this;

    return await this.request( url, params, options );
  }

  async request( url, params, options ) {
    const { method } = this._currentOptions;
    this.url( url );

    return await requestMethods[ this._format ]( method, this.url(), this._callbackName );
  }

  callbackName( callbackName ) {
    this._callbackName = callbackName;
    return this;
  }

  host( host ) {
    this._host = host;
    return this;
  }

  port( port ) {
    this._port = port;
    return this;
  }

  jsonp( jsonp ) {
    this._format = 'jsonp';
    return this;
  }

  json( json ) {
    this._format = 'json';
    return this;
  }

  protocol( protocol ) {
    return this._protocol;
  }

  url( url = null ) {
    if ( typeof url === 'string' ) {
      this._url = url;
      return this;
    }

    const { _port, _host } = this;

    return typeof _port === 'number'
      ? `${this._protocol}://${_host}:${_port}/${this._url}`
      : `${this._protocol}://${_host}/${this._url}`
  }

  http() {
    this._protocol = 'http';
    return this;
  }

  https() {
    this._protocol = 'https';
    return this;
  }

  _switchOptions( switchTo ) {
    this._currentOptions = {
      ...this._currentOptions,
      ...switchTo,
    }
  }
}

const makeRequest = (method, url) => {
  return new Promise(function (resolve, reject) {
    let xhr = new XMLHttpRequest();
    xhr.open(method, url);
    xhr.onload = function () {
      if (this.status >= 200 && this.status < 300) {
        resolve(xhr.response);
      } else {
        reject({
          status: this.status,
          statusText: xhr.statusText
        });
      }
    };
    xhr.onerror = function () {
      reject({
        status: this.status,
        statusText: xhr.statusText
      });
    };
    xhr.send();
  });
};

const makeJsonpRequest = (  ) => {
  let index = 0;
  const timeout = 5000;

  return ( method, url, callbackName ) => new Promise((resolve, reject) => {

    const callback = !callbackName ? `__callback${index++}` : callbackName;
    const timeoutID = window.setTimeout(() => {
      reject(new Error('Request timeout.'));
    }, timeout);

    window[callback] = response => {
      window.clearTimeout(timeoutID);
      resolve(response);
    };



    try {
      const script = document.createElement('script');
      script.type = 'text/javascript';
      script.async = true;
      script.src = `${url}${(url.indexOf('?') === -1 ? '?' : '&')}callback=${callback}`;

      document.getElementsByTagName('head')[0].appendChild(script);
    } catch( e ) {
        console.log(' WHY NOT ', e.message );
    }

  });
};

const requestMethods = {
  json: makeRequest,
  jsonp: makeJsonpRequest(),
};
