
export const AJAX_OPTIONS = {
  method: 'GET',
};

export const DELETE_AJAX_OPTIONS = {
  ...AJAX_OPTIONS,
  method: 'DELETE',
};

export const GET_AJAX_OPTIONS = {
  ...AJAX_OPTIONS,
  method: 'GET',
};

export const POST_AJAX_OPTIONS = {
  ...AJAX_OPTIONS,
  method: 'POST',
};

export const PUT_AJAX_OPTIONS = {
  ...AJAX_OPTIONS,
  method: 'PUT',
};
