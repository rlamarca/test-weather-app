
export const sfChronHost = 'www.sfchronicle.com/';

export const sfChronWeatherUrl =
  'external/weather/weather.json';

export const sfChronWeatherDataCallback = 'hdnWeatherJsonpCallback';

export const currentCityKey = 'palo_alto';
